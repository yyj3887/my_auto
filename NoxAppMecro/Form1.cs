﻿using OpenCvSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace NoxAppMecro
{
    public partial class Form1 : Form
    {
        
        public struct searchData
        {
            public double maxval;
            public int maxlocX;
            public int maxlocY;

            public searchData(double maxval , int maxlocX , int maxlocY)
            {
                this.maxval = maxval;
                this.maxlocX = maxlocX;
                this.maxlocY = maxlocY;
            }
        }

        public enum MECROSTATE
        {
            PLAY,
            PLAYING,
            ITEMPICKUP,
            RESTART,
            DIE
        }

        MECROSTATE state;        
        Stopwatch sw = new Stopwatch();
        //윈도우 창 크기및 위치 조정하는 기능
        [System.Runtime.InteropServices.DllImport("user32.dll", SetLastError = true)]
        internal static extern bool MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

        //앱 이름으로 윈도우 핸들얻어오는 기능
        [System.Runtime.InteropServices.DllImport("User32", EntryPoint = "FindWindow")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        //해당 윈도우 핸들내부 캡처 기능
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        internal static extern bool PrintWindow(IntPtr hWnd, IntPtr hdcBlt, int nFlags);

        //프로세스에 이벤트 메세지 전송 기능
        [System.Runtime.InteropServices.DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, IntPtr lParam);

        //자식윈도우핸들 가져오는 기능.
        [System.Runtime.InteropServices.DllImport("User32.dll")]
        public static extern IntPtr FindWindowEx(IntPtr Parent, IntPtr Child, string lpszClass, string lpszWindows);

        public const int WM_LBUTTONDOWN = 0x201;
        public const int WM_LBUTTONUP = 0x202;

        Bitmap screenImage = null;
        String AppPlayerName = "NoxPlayer";
                        
        Bitmap nomalitem = null;        
        Bitmap play = null;        
        Bitmap restart = null;        
        Bitmap itemPickup = null;
        Bitmap best = null;
        Bitmap Accept = null;         
        Bitmap exit_1 = null;
        Bitmap yes_1 = null;
        Bitmap die = null;
        Bitmap noclick = null;
        Bitmap ready = null;
        Bitmap tesoftop_end = null;
        Bitmap tesoftop_victory = null;
        Bitmap tesoftop_next = null;
        Bitmap tesoftop_die = null;
        Bitmap tesoftop_stop = null;

        IntPtr findwindow;
        IntPtr hwnd_child;
        public Form1()
        {
            InitializeComponent();                        
            nomalitem = new Bitmap(@"D:\MECRO\bin\노말.PNG");            
            play = new Bitmap(@"D:\MECRO\bin\play.PNG");            
            restart = new Bitmap(@"D:\MECRO\bin\다시하기.PNG");            
            best = new Bitmap(@"D:\MECRO\bin\best.PNG");
            itemPickup = new Bitmap(@"D:\MECRO\bin\bosang.PNG");
            Accept = new Bitmap(@"D:\MECRO\bin\event확인.PNG");                        
            exit_1 = new Bitmap(@"D:\MECRO\bin\itemgroup1\exit.PNG");
            yes_1 = new Bitmap(@"D:\MECRO\bin\itemgroup1\yes.PNG");
            die = new Bitmap(@"D:\MECRO\bin\die\die.PNG");
            noclick = new Bitmap(@"D:\MECRO\bin\die\no.PNG");
            ready = new Bitmap(@"D:\MECRO\bin\die\ready.PNG");
            tesoftop_end = new Bitmap(@"D:\MECRO\bin\시험의탑\시험의탑.PNG");
            tesoftop_victory = new Bitmap(@"D:\MECRO\bin\시험의탑\보상.PNG");
            tesoftop_next = new Bitmap(@"D:\MECRO\bin\시험의탑\시험의탑다음층.PNG");
            tesoftop_die = new Bitmap(@"D:\MECRO\bin\시험의탑\패배.PNG");
            tesoftop_stop = new Bitmap(@"D:\MECRO\bin\시험의탑\전투준비.PNG");

            findwindow = FindWindow(null,AppPlayerName);
            hwnd_child = FindWindowEx(findwindow, IntPtr.Zero, "Qt5QWindowIcon", "ScreenBoardClassWindow");
            MoveWindow(findwindow, 0, 0, 1118, 669,false);
            state = MECROSTATE.PLAY;
            radio_standad.Checked = true;


        }

        //x,y 값을 전달해주면 클릭이벤트를 발생합니다.
        public void InClick(int x, int y)
        {
            ////클릭이벤트를 발생시킬 플레이어를 찾습니다.
            //IntPtr findwindow = FindWindow(null, AppPlayerName);
            if (findwindow != IntPtr.Zero)
            {
                //플레이어를 찾았을 경우 클릭이벤트를 발생시킬 핸들을 가져옵니다.
                //IntPtr hwnd_child = FindWindowEx(findwindow, IntPtr.Zero, "Qt5QWindowIcon","ScreenBoardClassWindow");
                IntPtr lparam = new IntPtr(x | (y << 16));

                //Cursor.Position = new System.Drawing.Point(x, y);

                //플레이어 핸들에 클릭 이벤트를 전달합니다.
                SendMessage(hwnd_child, WM_LBUTTONDOWN, 1, lparam);
                SendMessage(hwnd_child, WM_LBUTTONUP, 0, lparam);
            }
        }


        //searchIMG에 스크린 이미지와 찾을 이미지를 넣어줄거에요
        public searchData GetsearchIMGData(Bitmap screen_img, Bitmap find_img)
        {
            //찾은 이미지의 유사도를 담을 더블형 최대 최소 값을 선언합니다.
            double minval, maxval = 0;
            //찾은 이미지의 위치를 담을 포인트형을 선업합니다.
            OpenCvSharp.Point minloc, maxloc;

            //스크린 이미지 선언
            using (Mat ScreenMat = OpenCvSharp.Extensions.BitmapConverter.ToMat(screen_img))
            //찾을 이미지 선언
            using (Mat FindMat = OpenCvSharp.Extensions.BitmapConverter.ToMat(find_img))                
            //스크린 이미지에서 FindMat 이미지를 찾아라
            using (Mat res = ScreenMat.MatchTemplate(FindMat, TemplateMatchModes.CCoeffNormed))
            {                                
                //찾은 이미지의 유사도 및 위치 값을 받습니다. 
                Cv2.MinMaxLoc(res, out minval, out maxval, out minloc, out maxloc);
                Debug.WriteLine("찾은 이미지의 유사도 : " + maxval);
                res.Dispose();
                ScreenMat.Dispose();
                FindMat.Dispose();
            }

            GC.Collect();

            return new searchData(maxval, maxloc.X, maxloc.Y);                
        }

        public double GetsearchMaxval(Bitmap screen_img, Bitmap find_img)
        {
            //찾은 이미지의 유사도를 담을 더블형 최대 최소 값을 선언합니다.
            double minval, maxval = 0;
            //스크린 이미지 선언
            using (Mat ScreenMat = OpenCvSharp.Extensions.BitmapConverter.ToMat(screen_img))
            //찾을 이미지 선언
            using (Mat FindMat = OpenCvSharp.Extensions.BitmapConverter.ToMat(find_img))
            //스크린 이미지에서 FindMat 이미지를 찾아라
            using (Mat res = ScreenMat.MatchTemplate(FindMat, TemplateMatchModes.CCoeffNormed))
            {
                //찾은 이미지의 위치를 담을 포인트형을 선업합니다.
                OpenCvSharp.Point minloc, maxloc;
                //찾은 이미지의 유사도 및 위치 값을 받습니다. 
                Cv2.MinMaxLoc(res, out minval, out maxval, out minloc, out maxloc);
                Debug.WriteLine("찾은 이미지의 유사도 : " + maxval);
                res.Dispose();
                ScreenMat.Dispose();
                FindMat.Dispose();

            }

            GC.Collect();

            return maxval;
        }

        private void captureImage()
        {            
            if (findwindow != IntPtr.Zero)
            {
                //플레이어를 찾았을 경우
                Debug.WriteLine("앱플레이어 찾았습니다.");

                //찾은 플레이어를 바탕으로 Graphics 정보를 가져옵니다.
                Graphics Graphicsdata = Graphics.FromHwnd(findwindow);

                //찾은 플레이어 창 크기 및 위치를 가져옵니다. 
                Rectangle rect = Rectangle.Round(Graphicsdata.VisibleClipBounds);

                //플레이어 창 크기 만큼의 비트맵을 선언해줍니다.
                Bitmap bmp = new Bitmap(rect.Width, rect.Height);

                //비트맵을 바탕으로 그래픽스 함수로 선언해줍니다.
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    //찾은 플레이어의 크기만큼 화면을 캡쳐합니다.
                    IntPtr hdc = g.GetHdc();
                    PrintWindow(findwindow, hdc, 0x2);
                    g.ReleaseHdc(hdc);
                }

                // pictureBox1 이미지를 표시해줍니다.
                screenImage = bmp;                
            }
            else
            {
                //플레이어를 못찾을경우
                Debug.WriteLine("못찾았어요");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (radio_standad.Checked)
            {
                StdContent();
            }
            else
            {
                TestOfToPContent();
            }
        }

        private void StdContent()
        {
            if (state == MECROSTATE.PLAY)
            {
                captureImage();
                searchData sd = GetsearchIMGData(screenImage, play);
                if (sd.maxval > 0.9)
                {
                    if (state == MECROSTATE.PLAY)
                    {
                        statetextBox1.Text = "시작중 ....";
                        state = MECROSTATE.PLAYING;
                        InClick(sd.maxlocX, sd.maxlocY);
                    }
                    return;
                }
                else
                {
                    Debug.WriteLine("not find");
                }

            }

            if (state == MECROSTATE.PLAYING)
            {
                captureImage();
                //3초마다 victory 이미지 서치
                searchData bestsd = GetsearchIMGData(screenImage, best);
                //전투가 종료되면 victory 이미지가 나타난다.
                if (bestsd.maxval > 0.9)
                {                    
                    statetextBox1.Text = "아이템 픽업중....";
                    InClick(bestsd.maxlocX, bestsd.maxlocY);


                    searchData exit_data = GetsearchIMGData(screenImage, exit_1);

                    if (exit_data.maxval > 0.9)
                    {
                        searchData nomal_data = GetsearchIMGData(screenImage, nomalitem);
                        //노말아이템일때
                        if (nomal_data.maxval > 0.9)
                        {                            
                                //판매 클릭
                            InClick(470,500);                                                        
                            state = MECROSTATE.RESTART;
                            return;                            
                        }
                        
                        //특이사항 없을때 그냥 확인버튼 클릭
                        InClick(650, 500);
                        state = MECROSTATE.RESTART;
                        return;

                    }

                }
                else
                {
                    statetextBox1.Text = "전투중 ...";
                }

                searchData diesearchData = GetsearchIMGData(screenImage, die);

                if (diesearchData.maxval > 0.8)
                {
                    state = MECROSTATE.DIE;
                    statetextBox1.Text = "패배...";
                    return;
                }
            }

            if (state == MECROSTATE.RESTART)
            {

                statetextBox1.Text = "다시 시작중...";
                captureImage();
                searchData sd = GetsearchIMGData(screenImage, restart);
                if (sd.maxval > 0.7)
                {
                    InClick(sd.maxlocX, sd.maxlocY);
                    state = MECROSTATE.PLAYING;
                    return;
                }
                else
                {
                    //다시시작이 나와야하는데 이벤트 아이템이 나올경우도 있음 예외처리
                    searchData eventAppcetsd = GetsearchIMGData(screenImage, Accept);
                    if (eventAppcetsd.maxval > 0.8)
                    {
                        InClick(eventAppcetsd.maxlocX, eventAppcetsd.maxlocY);
                    }

                    //판매를 눌렀음애도 불구하고 10000만원 이상일때 다시 팝업뜸
                    searchData yes_1Data = GetsearchIMGData(screenImage, yes_1);
                    if (yes_1Data.maxval > 0.8)
                    {
                        InClick(yes_1Data.maxlocX, yes_1Data.maxlocY);
                    }
                }
            }

            if (state == MECROSTATE.DIE)
            {
                captureImage();
                searchData noclickSearchData = GetsearchIMGData(screenImage, noclick);
                if (noclickSearchData.maxval > 0.9)
                {
                    InClick(noclickSearchData.maxlocX, noclickSearchData.maxlocY);
                    return;
                }

                InClick(500, 50);

                searchData readysearchData = GetsearchIMGData(screenImage, ready);

                if (readysearchData.maxval > 0.9)
                {
                    InClick(readysearchData.maxlocX, readysearchData.maxlocY);
                    state = MECROSTATE.PLAY;
                    return;
                }

            }
        }
        private void TestOfToPContent()
        {
            if (state == MECROSTATE.PLAY)
            {
                captureImage();
                searchData sd = GetsearchIMGData(screenImage, play);
                if (sd.maxval > 0.9)
                {
                    if (state == MECROSTATE.PLAY)
                    {
                        statetextBox1.Text = "시작중 ....";
                        state = MECROSTATE.PLAYING;
                        InClick(sd.maxlocX, sd.maxlocY);
                    }
                    return;
                }
                else
                {
                    Debug.WriteLine("not find");
                }

            }

            if (state == MECROSTATE.PLAYING)
            {
                captureImage();
                //3초마다 victory 이미지 서치
                searchData tesoftop_victory_data = GetsearchIMGData(screenImage, tesoftop_victory);
                //전투가 종료되면 시험 이미지가 나타난다.
                if (tesoftop_victory_data.maxval > 0.9)
                {
                    //timer1.Interval = 5000;
                    statetextBox1.Text = "아이템 픽업중....";
                    InClick(557,482);

                    searchData tesoftop_victory_next = GetsearchIMGData(screenImage, tesoftop_next);

                    if (Math.Round(tesoftop_victory_next.maxval,1) >= 0.9)
                    {                                                
                            InClick(354,342);
                            state = MECROSTATE.PLAY;
                            return;                        
                    }
                }
                else
                {
                    statetextBox1.Text = "전투중 ...";
                }

                searchData diesearchData = GetsearchIMGData(screenImage, tesoftop_die);

                if (diesearchData.maxval > 0.9)
                {
                    //state = MECROSTATE.DIE;                    

                    searchData tesoftop_stop_data = GetsearchIMGData(screenImage, tesoftop_stop);

                    if (tesoftop_stop_data.maxval > 0.8)
                    {
                        InClick(354, 342);
                        state = MECROSTATE.DIE;
                        statetextBox1.Text = "전투대기 덱 재구성...";
                        return;
                    }

                    statetextBox1.Text = "패배...";
                    InClick(354, 342);
                    return;
                }
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            if(comboBox1.SelectedItem.ToString() == "초기화")
            {
                statetextBox1.Text = "초기화됨 멈춤...";
                timer1.Enabled = false;
                state = MECROSTATE.PLAY;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cboDebuging.SelectedItem.ToString() == MECROSTATE.PLAY.ToString())
            {
                statetextBox1.Text = "PLAY...";                
                state = MECROSTATE.PLAY;
            }
            else if (cboDebuging.SelectedItem.ToString() == MECROSTATE.PLAYING.ToString())
            {
                statetextBox1.Text = "PLAYING...";                
                state = MECROSTATE.PLAYING;
            }
            else if (cboDebuging.SelectedItem.ToString() == MECROSTATE.ITEMPICKUP.ToString())
            {
                statetextBox1.Text = "ITEMPICKUP...";                
                state = MECROSTATE.ITEMPICKUP;
            }
            else if (cboDebuging.SelectedItem.ToString() == MECROSTATE.RESTART.ToString())
            {
                statetextBox1.Text = "RESTART...";                
                state = MECROSTATE.RESTART;
            }
            else if (cboDebuging.SelectedItem.ToString() == MECROSTATE.DIE.ToString())
            {
                statetextBox1.Text = "DIE...";                
                state = MECROSTATE.DIE;
            }

            timer1.Enabled = true;
        }
    }
}
